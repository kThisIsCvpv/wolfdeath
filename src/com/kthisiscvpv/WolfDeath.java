package com.kthisiscvpv;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class WolfDeath extends JavaPlugin implements Listener {

	public void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
	}

	public void onDisable() {

	}

	@EventHandler
	public void onDeath(PlayerDeathEvent evt) {
		Player p = evt.getEntity();
		p.getWorld().playSound(p.getLocation(), Sound.WOLF_HOWL, 1F, 0F);
	}
}
